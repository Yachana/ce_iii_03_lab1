def linearSearch(values,target):
    n=len(values)
    for i in range(n):
    	if values[i]==target:
        	return i
    return -1  

def binarySearch(values,target):
	low = 0
	high=len(values)-1
	while low<=high:
		midPoint = (low+high)//2
		if(values[midPoint] == target):
			return midPoint
		else:
			if(values[midPoint]>target):
				high = midPoint-1
			else:
				low = midPoint+1
	return -1







       
