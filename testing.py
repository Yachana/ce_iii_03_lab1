import unittest
from search import linearSearch
from search import binarySearch

class SearchTestCase(unittest.TestCase):
# Linear Search Test
	def test_linear_search(self):
		arr=[9,8,4,2,5,0,1,2,3]
		self.assertEqual(linearSearch(arr,9),0)	#5 lies in the 0th index so no error
		self.assertEqual(linearSearch(arr,5),4)
		self.assertEqual(linearSearch(arr,10),-1)

# Binary Search Test
	def test_binary_search(self):
		#sorted array for binary search
		arr=[2,3,0,6,4,5,6,7,9]
		self.assertEqual(binarySearch(arr,7),7)
		self.assertEqual(binarySearch(arr,3),1)
		self.assertEqual(binarySearch(arr,10),-1)

if __name__ == '__main__':
	unittest.main()
